import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { CarsComponent } from './cars/cars.component';
import { AnimalsComponent } from './animals/animals.component';
import { FlowersComponent } from './flowers/flowers.component';

@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    CarsComponent,
    AnimalsComponent,
    FlowersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
