import { Component, OnInit } from '@angular/core';
import { Options } from 'selenium-webdriver/ie';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  options = [];
  showSubMenu = false;

  constructor() {}

  ngOnInit() {
    console.log(window.location.pathname)
    if (window.location.pathname === '/animals') {
      this.fillOptions('animals');
    } else if (window.location.pathname === '/cars') {
      this.fillOptions('cars');
    } else if (window.location.pathname === '/flowers') {
      this.fillOptions('flowers');
    }
  }

  openSubMenu (event) {
    if (event.target.href.indexOf('animals') > -1) {
      this.fillOptions('animals');
    } else if (event.target.href.indexOf('cars') > -1) {
      this.fillOptions('cars');
    } else if (event.target.href.indexOf('flowers') > -1) {
      this.fillOptions('flowers');
    }
  }

  fillOptions(params: string) {
    switch(params) {
      case 'animals':
        this.options = ['Tiger', 'Shark', 'Dolphin'];
        this.showSubMenu = true;
        break;
      case 'cars':
        this.options = ['BMW', 'Mersedes', 'Toyota'];
        this.showSubMenu = true;
        break;
      case 'flowers':
        this.options = ['Rose', 'Lily', 'Magnolia'];
        this.showSubMenu = true;
        break;
      default:
        console.log('No such values');
    }
  }


}
