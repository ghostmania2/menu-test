import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimalsComponent } from './animals/animals.component';
import { CarsComponent } from './cars/cars.component';
import { FlowersComponent } from './flowers/flowers.component';

const routes: Routes = [
  {path: 'animals', component: AnimalsComponent},
  {path: 'cars', component: CarsComponent},
  {path: 'flowers', component: FlowersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
